FROM golang:1.9.2
WORKDIR /
ADD main.go index.html aviator.png /
RUN go get -d -v .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o aviator .
CMD ["/aviator"]
